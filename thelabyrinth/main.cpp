#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <algorithm>
#include <iomanip>
#include <queue>
#include <string>
#include <math.h>
#include <ctime>

using namespace std;

bool foundC = false;
bool returning = false;
vector<string> returnPath;
vector<pair<int,int>> explored;
vector<pair<int,int>> unexplored;

const int dir=4; // number of possible directions to go at any position
static int dx[dir]={1, 0, -1, 0};
static int dy[dir]={0, 1, 0, -1};

//A* from http://code.activestate.com/recipes/577457-a-star-shortest-path-algorithm/

class node
{
    // current position
    int xPos;
    int yPos;
    // total distance already travelled to reach the node
    int level;
    // priority=level+remaining distance estimate
    int priority;  // smaller: higher priority
    
public:
    node(int xp, int yp, int d, int p)
    {xPos=xp; yPos=yp; level=d; priority=p;}
    
    int getxPos() const {return xPos;}
    int getyPos() const {return yPos;}
    int getLevel() const {return level;}
    int getPriority() const {return priority;}
    
    void updatePriority(const int & xDest, const int & yDest)
    {
        priority=level+estimate(xDest, yDest)*10; //A*
    }
    
    // give better priority to going strait instead of diagonally
    void nextLevel(const int & i) // i: direction
    {
        level+=(dir==8?(i%2==0?10:14):10);
    }
    
    // Estimation function for the remaining distance to the goal.
    const int & estimate(const int & xDest, const int & yDest) const
    {
        static int xd, yd, d;
        xd=xDest-xPos;
        yd=yDest-yPos;
        
        // Euclidian Distance
        d=static_cast<int>(sqrt(xd*xd+yd*yd));
        
        // Manhattan distance
        //d=abs(xd)+abs(yd);
        
        // Chebyshev distance
        //d=max(abs(xd), abs(yd));
        
        return(d);
    }
};

// Determine priority (in the priority queue)
bool operator<(const node & a, const node & b)
{
    return a.getPriority() > b.getPriority();
}

// A-star algorithm.
// The route returned is a string of direction digits.
string pathFind(
                const int n,
                const int m,
                vector<vector<int>> mapVect,
                const int & xStart,
                const int & yStart,
                const int & xFinish,
                const int & yFinish
                )
{
    int map[n][m];
    int closed_nodes_map[n][m]; // map of closed (tried-out) nodes
    int open_nodes_map[n][m]; // map of open (not-yet-tried) nodes
    int dir_map[n][m]; // map of directions
    
    for(auto i=0; i<n; i++) {
        for(auto j=0; j<m; j++) {
            map[i][j] = mapVect[i][j];
        }
    }
    
    static priority_queue<node> pq[2]; // list of open (not-yet-tried) nodes
    static int pqi; // pq index
    static node* n0;
    static node* m0;
    static int i, j, x, y, xdx, ydy;
    static char c;
    pqi=0;
    
    // reset the node maps
    for(y=0;y<m;y++)
    {
        for(x=0;x<n;x++)
        {
            closed_nodes_map[x][y]=0;
            open_nodes_map[x][y]=0;
        }
    }
    
    // create the start node and push into list of open nodes
    n0=new node(xStart, yStart, 0, 0);
    n0->updatePriority(xFinish, yFinish);
    pq[pqi].push(*n0);
    open_nodes_map[x][y]=n0->getPriority(); // mark it on the open nodes map
    
    // A* search
    while(!pq[pqi].empty())
    {
        // get the current node w/ the highest priority
        // from the list of open nodes
        n0=new node( pq[pqi].top().getxPos(), pq[pqi].top().getyPos(),
                    pq[pqi].top().getLevel(), pq[pqi].top().getPriority());
        
        x=n0->getxPos(); y=n0->getyPos();
        
        pq[pqi].pop(); // remove the node from the open list
        open_nodes_map[x][y]=0;
        // mark it on the closed nodes map
        closed_nodes_map[x][y]=1;
        
        // quit searching when the goal state is reached
        //if((*n0).estimate(xFinish, yFinish) == 0)
        if(x==xFinish && y==yFinish)
        {
            // generate the path from finish to start
            // by following the directions
            string path="";
            while(!(x==xStart && y==yStart))
            {
                j=dir_map[x][y];
                c='0'+(j+dir/2)%dir;
                path=c+path;
                x+=dx[j];
                y+=dy[j];
            }
            
            // garbage collection
            delete n0;
            // empty the leftover nodes
            while(!pq[pqi].empty()) pq[pqi].pop();
            return path;
        }
        
        // generate moves (child nodes) in all possible directions
        for(i=0;i<dir;i++)
        {
            xdx=x+dx[i]; ydy=y+dy[i];
            
            if(!(xdx<0 || xdx>n-1 || ydy<0 || ydy>m-1 || map[xdx][ydy]==1
                 || closed_nodes_map[xdx][ydy]==1))
            {
                // generate a child node
                m0=new node( xdx, ydy, n0->getLevel(),
                            n0->getPriority());
                m0->nextLevel(i);
                m0->updatePriority(xFinish, yFinish);
                
                // if it is not in the open list then add into that
                if(open_nodes_map[xdx][ydy]==0)
                {
                    open_nodes_map[xdx][ydy]=m0->getPriority();
                    pq[pqi].push(*m0);
                    // mark its parent node direction
                    dir_map[xdx][ydy]=(i+dir/2)%dir;
                }
                else if(open_nodes_map[xdx][ydy]>m0->getPriority())
                {
                    // update the priority info
                    open_nodes_map[xdx][ydy]=m0->getPriority();
                    // update the parent direction info
                    dir_map[xdx][ydy]=(i+dir/2)%dir;
                    
                    // replace the node
                    // by emptying one pq to the other one
                    // except the node to be replaced will be ignored
                    // and the new node will be pushed in instead
                    while(!(pq[pqi].top().getxPos()==xdx &&
                            pq[pqi].top().getyPos()==ydy))
                    {
                        pq[1-pqi].push(pq[pqi].top());
                        pq[pqi].pop();
                    }
                    pq[pqi].pop(); // remove the wanted node
                    
                    // empty the larger size pq to the smaller one
                    if(pq[pqi].size()>pq[1-pqi].size()) pqi=1-pqi;
                    while(!pq[pqi].empty())
                    {
                        pq[1-pqi].push(pq[pqi].top());
                        pq[pqi].pop();
                    }
                    pqi=1-pqi;
                    pq[pqi].push(*m0); // add the better node instead
                }
                else delete m0; // garbage collection
            }
        }
        delete n0; // garbage collection
    }
    return ""; // no route found
}


vector<string> findPath(const int R, const int C, int SR, int SC, int FR, int FC, const vector<string>ROW, bool incUnknown) {
    //SR, SC is start, FR FC is finish
    
    vector<vector<int>> map (C, vector<int>(R, 0));
    
    //find way back from control room to start
    //0=open,1=obstacle,2=start,3=path,4=finish
    for(auto y=0; y<R; y++) {
        for(auto x=0; x<C; x++) {
            string cur = string(1, ROW[y][x]);
            if(cur == "T" && FR < 0 && FC < 0) {
                //start at control room and go back
                FR = y;
                FC = x;
            } else if(cur == "C" && SR < 0 && SC < 0) {
                //finish at the starting point
                SR = y;
                SC = x;
            } else if(cur == "#") {
                //obstacle
                map[x][y] = 1;
            } else if(cur == "?" && !incUnknown) {
                //unknown, ok only if we checked it as explored or unexplored
                map[x][y] = 1;
            }
        }
    }
    
    cerr<<"Map Size (X,Y): "<<C<<","<<R<<endl;
    cerr<<"Start: "<<SC<<","<<SR<<endl;
    cerr<<"Finish: "<<FC<<","<<FR<<endl;
    // get the route
    clock_t start = clock();
    string route=pathFind(C, R, map, SC, SR, FC, FR);
    
    if(route=="") cerr<<"An empty route generated!"<<endl;
    clock_t end = clock();
    double time_elapsed = double(end - start);
    cerr<<"Time to calculate the route (ms): "<<time_elapsed<<endl;
    cerr<<"Route:"<<endl;
    cerr<<route<<endl<<endl;
    
    // follow the route on the map and display it
    if(route.length() > 72) {
        //try again more agressively
        return findPath(R, C, SR, SC, FR, FC, ROW, true);
    }
    if(route.length()>0)
    {
        int j;
        //char c;
        int x=SC;
        int y=SR;
        map[x][y]=2;
        for(int i=0;i<route.length();i++)
        {
            //c=route.at(i);
            string c = string(1, route[i]);
            j=atoi(c.c_str());
            
            x=x+dx[j];
            y=y+dy[j];
            map[x][y]=3;
        }
        map[x][y]=4;
        
        // display the map with the route
        for(int y=0;y<R;y++)
        {
            for(int x=0;x<C;x++)
                if(map[x][y]==0)
                    cerr<<"?";
                else if(map[x][y]==1)
                    cerr<<"#"; //obstacle
                else if(map[x][y]==2)
                    cerr<<"C"; //start
                else if(map[x][y]==3) {
                    cerr<<"."; //route
                } else if(map[x][y]==4)
                    cerr<<"T"; //finish
            cerr<<endl;
        }
    }
    vector<string> ret;
    auto len = (int)route.length();
    for (int i=len-1; i>=0; i--) {
        auto cur = string(1, route[i]);
        //0=left,1=up,2=right,3=down
        if(cur == "2") {
            cerr << cur << " LEFT";
            ret.push_back("LEFT");
        }
        if(cur == "3") {
            cerr << cur << " UP";
            ret.push_back("UP");
        }
        if(cur == "0") {
            cerr << cur << " RIGHT";
            ret.push_back("RIGHT");
        }
        if(cur == "1") {
            cerr << cur << " DOWN";
            ret.push_back("DOWN");
        }
        cerr << " " << i << ",";
    }
    cerr << endl;
    
    return ret;
}

bool updateExplored(bool found, pair<int,int> pt)
{
    if(found) {
        //already found an unexplored point, make a note to maybe check this later
        if(find(unexplored.begin(), unexplored.end(), pt) == unexplored.end()) {
            unexplored.push_back(pt);
        }
    } else {
        //we're going here now, be sure to remove it from unexplored
        for(auto it=unexplored.begin(); it<unexplored.end(); it++) {
            auto curPt = *it;
            if(curPt == pt) {
                unexplored.erase(it);
                break;
            }
        }
        explored.push_back(pt);
        found = true;
    }
    return found;
}

bool isUnexplored(const int R, const int C, const pair<int,int> pt, const vector<string> ROW)
{
    return (
            pt.first >= 0 && pt.first < C &&
            pt.second >= 0 && pt.second < R &&
            string(1, ROW[pt.second][pt.first]) == "." &&
            find(explored.begin(), explored.end(), pt) == explored.end()
            );
}

string findNextMove(const int R, const int C, const int KR, const int KC, const vector<string> ROW)
{
    
    if(returnPath.size() > 0) {
        //we couldn't find unexplored, going back to the last point where one exists
        string next = returnPath.back();
        returnPath.pop_back();
        returning = true;
        return next;
    }
    
    returning = false;
    string ret = "";
    string curRow = ROW[KR];
    
    //go to control room if possible
    if(KC < C - 1 && string(1, curRow[KC+1]) == "C") {
        foundC = true;
        explored.push_back(make_pair(KC+1, KR));
        ret = "RIGHT";
    }
    if(KR < R - 1 && string(1, ROW[KR+1][KC]) == "C") {
        foundC = true;
        explored.push_back(make_pair(KC, KR+1));
        ret = "DOWN";
    }
    if(KC > 0 && string(1, curRow[KC-1]) == "C") {
        explored.push_back(make_pair(KC-1, KR));
        foundC = true;
        ret = "LEFT";
    }
    if(KR > 0 && string(1, ROW[KR-1][KC]) == "C") {
        explored.push_back(make_pair(KC, KR-1));
        foundC = true;
        ret = "UP";
    }
    if(foundC) {
        cerr << "FOUND C!" << endl;
        //explored = {};
        //unexplored = {};
        returnPath = findPath(R, C, -1, -1, -1, -1, ROW, false);
        return ret;
    }
    
    //next look around for unexplored points
    auto dir = "";
    auto found = false;
    auto pt = make_pair(KC-1, KR);
    if(isUnexplored(R, C, pt, ROW)) {
        if(!found) dir = "LEFT";
        found = updateExplored(found, pt);
    }
    
    pt = make_pair(KC, KR-1);
    if(isUnexplored(R, C, pt, ROW)) {
        if(!found) dir = "UP";
        found = updateExplored(found, pt);
    }
    
    pt = make_pair(KC+1, KR);
    if(isUnexplored(R, C, pt, ROW)) {
        if(!found) dir = "RIGHT";
        found = updateExplored(found, pt);
    }
    
    pt = make_pair(KC, KR+1);
    if(isUnexplored(R, C, pt, ROW)) {
        if(!found) dir = "DOWN";
        found = updateExplored(found, pt);
    }
    
    if(found) {
        return dir;
    }
    
    //no unexplored neighbors, see if there was one previously
    if(!unexplored.empty()) {
        auto lastUnexplored = unexplored.back();
        unexplored.pop_back();
        auto now = make_pair(KC, KR);
        while(lastUnexplored == now) {
            lastUnexplored = unexplored.back();
            unexplored.pop_back();
        }
        //move point to "explored"
        explored.push_back(lastUnexplored);
        //go back to the point
        returning = true;
        returnPath = findPath(R, C, KR, KC, lastUnexplored.second, lastUnexplored.first, ROW, false);
        string next = returnPath.back();
        returnPath.pop_back();
        return next;
    }
    
    //should never come here
    cerr << "SHOULD NEVER COME HERE " << unexplored.size() << endl;
    returning = false;
    return "BAD";
}

int main(int argc, char* argv[])
{
    foundC = false;
    const int R = 15; // number of rows.
    const int C = 30; // number of columns.
    //const int A = 7; // number of rounds between the time the alarm countdown is activated and the time the alarm goes off.
    
    
    /*
    foundC = true;
    const int KR = -1; // row where Kirk is located.
    const int KC = -1; // column where Kirk is located.
    const vector<string> ROW = {
        "#################?????????????",
        "#T...............?????????????",
        "##...............?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#C...............?????????????",
        "#################?????????????"
    };
     */
    
    
    const int KR = 12; // row where Kirk is located.
    const int KC = 1; // column where Kirk is located.
    const vector<string> ROW = {
        "#################?????????????",
        "#................?????????????",
        "##...............?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#................?????????????",
        "#C...........T...?????????????",
        "#################?????????????"
    };
    
    
    
    
    if(foundC) {
        if(returnPath.size() > 0) {
            cerr << returnPath.back() << endl;
            returnPath.pop_back();
        }
    } else {
        //pick random way to move
        cerr << findNextMove(R, C, KR, KC, ROW) << endl;
        if(!foundC && !returning) {
            //re-init path home
            returnPath = {};
        }
    }
    
    /*
    // game loop
    int R; // number of rows.
    int C; // number of columns.
    int A; // number of rounds between the time the alarm countdown is activated and the time the alarm goes off.
    cin >> R >> C >> A; cin.ignore();
    
    // game loop
    while (1) {
        int KR; // row where Kirk is located.
        int KC; // column where Kirk is located.
        cin >> KR >> KC; cin.ignore();
        vector<string> rows;
        for (int i = 0; i < R; i++) {
            string ROW; // C of the characters in '#.TC?' (i.e. one line of the ASCII maze).
            cin >> ROW; cin.ignore();
            //cout << ROW << endl;
            for(auto j=0; j<C; j++) {
                auto pt = make_pair(j,i);
                string cur = string(1, ROW[j]);
                if(find(explored.begin(), explored.end(), pt) != explored.end() && cur == ".") {
                    cout<<"@";
                } else {
                    cout<<ROW[j];
                }
            }
            cout << endl;
            rows.push_back(ROW);
        }
     
        if(foundC) {
            if(returnPath.size() > 0) {
                cout << "going back " << returnPath.size() << endl;
                cout << returnPath.back() << endl;
                returnPath.pop_back();
            }
        } else {
            cout << "finding control room" << endl;
            //pick random way to move
            cout << findNextMove(R, C, KR, KC, rows) << endl;
            if(!foundC && !returning) {
                //re-init path home
                returnPath = {};
            }
        }
        */
    return(0);
    
}
